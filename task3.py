#!/usr/bin/env python3

import os
import sys
import operations

def instructions():
    print('####  Operations:  ####')
    print('Create file: 1')
    print('Read file: 2')
    print('Write into file: 3')
    print('Delete file: 4')
    print('Create dir: 5')
    print('Delete dir: 6')
    print('Exit program now: 7')
    print('Choose operation -->',end="")    
    
def checkinput(b):
    if not 1 <= b < 8:
        print('Invalid operation! Please enter correct number of operation.')
    elif b == 7:
        print('Closing program! Goodbye!')
        return False
        sys.exit
    else:
        return True

def startprogram():
    a=0
    while a!=7:
        instructions()
        try:
            a=int(input())
        except ValueError:
            a=0
        if checkinput(a):
            if a==1:
                operations.createfile()
            elif a==2:
                operations.readfile()
            elif a==3:
                operations.writefile()
            elif a==4:
                operations.deletefile()
            elif a==5:
                operations.createdir()
            elif a==6:
                operations.deletedir()

#Начало выполнения программы
startprogram()
