#!/usr/bin/env python3
from os import remove
from os import mkdir
from os import rmdir
from os import walk
from shutil import rmtree
from os.path import exists
from os.path import isdir
from os.path import isfile
from os.path import abspath

def checkobject(path):
    if exists(abspath(path)):
       if isfile(abspath(path)) or isdir(abspath(path)):
          return True
    else:
        return False
        
def testdir(path):
    a=0
    for address, dirs, files in walk(path):
        if address or files:
            a+=1
            if int(a)>0:
                return True
            else:
                return False

def createfile():
    print('Enter filepath -->',end="")
    filename=input()
    if checkobject(filename):
        print('File already exists. Overwrite file?')
        Flag=False
        while Flag==False:
            print('yes/no ->',end='')
            p=input()
            if p == 'yes':
                Flag=True
            elif p == 'no':
                Flag=True
                return
    file=open(filename,'w')
    file.close()
    print('File',abspath(filename),'created successfully!')
    
def readfile():
    print('Enter filepath -->',end="")
    filename=input()
    if checkobject(filename):
       file=open(filename)
       print(abspath(filename))
       print('')
       print(file.read())
       print('')
       file.close()
    else:
        print('File does not exists!')
    
def writefile():
    print('Enter filepath -->',end="")
    filename=input()
    if checkobject(filename):
       print(abspath(filename),'\n')
       file=open(filename,'a+')
       print('Enter text and press ENTER for complete')
       s=input()
       file.write(s)
       file.close()
       print('Save text in file successfully!')
    else:
        print('File does not exists!')
    
def deletefile():
    print('Enter filepath -->',end="")
    filename=input()
    if checkobject(filename):
        if isdir(abspath(filename))==False:
            print('File',abspath(filename),end='')
            remove(abspath(filename))
            print(' deleted successfully!')
        else:
            print('Is not file entered!')
            return
    else:
        print('File does not exists!')
             
    
def createdir():
    print('Enter directory path -->')
    dirpath=input()
    if checkobject(dirpath):
        if isdir(abspath(dirpath)):
            print('Directory already exists. Overwrite dir?')
            Flag=False
            while Flag==False:
                print('yes/no ->',end='')
                p=input()
                if p == 'yes':
                    rmtree(dirpath)
                    Flag=True
                elif p == 'no':
                    Flag=True
                    return
    mkdir(dirpath)
    print('Directory',abspath(dirpath),' created successfully!')
    
def deletedir():
    print('Enter directory path -->')
    dirpath=input()
    if checkobject(dirpath):
        if isdir(abspath(dirpath)) and testdir(abspath(dirpath)):
            print('Directory is not empty. Delete dir?')
            Flag=False
            while Flag==False:
                print('yes/no ->',end='')
                p=input()
                if p == 'yes':
                    rmtree(dirpath)
                    Flag=True
                    print('Directory', abspath(dirpath), ' deleted successfully!')
                    return
                elif p == 'no':
                    Flag=True
                    print('Delete canceled!')
                    return
            rmdir(dirpath)
            print('Directory', abspath(dirpath), ' deleted successfully!')

