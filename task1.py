#!/usr/bin/env python3

import sys
import os

class mypath:
    
    def enter_data(self):
        global p
        if len(sys.argv) > 1:
            p=sys.argv[1]
        else: 
            print ('Enter path -->')
            p=input()
            if len(p) < 1:
                p=os.path.dirname(os.path.abspath(__file__))
    
    def valid_data(self):
        if os.path.exists(p) == False:
             print('Wrong path or directory not exist')
             sys.exit()
        else:
            if os.path.isdir(p) == False:
                print('This path not a directory')
                sys.exit()
    
    def show_result(self):
        for name in os.listdir(p):
            if os.path.islink(os.path.join(p,name)):
                   print(name + '  link')
            elif os.path.isfile(os.path.join(p,name)):
                print(name+'  file')
            elif os.path.isdir(os.path.join(p,name)):
                   print(name+'  dir')

c = mypath()
c.enter_data()
c.valid_data()
c.show_result()